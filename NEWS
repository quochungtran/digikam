digiKam 8.0.0 - Release date: 2022-xx-xx

*****************************************************************************************************
NEW FEATURES:

General   : Port code to Qt6 and KF6 framework.
General   : Add JPEG-XL, WEBP, and AVIF export settings everywhere.
            Add convert to lossless JPEG-XL, WEBP, and AVIF while importing from camera.
            Add JPEG-XL; WEBP, and AVIF converters in Batch Queue Manager.
Versioning: Support JPEG-XL, HEIF, WEBP, and AVIF as lossless image format to store versionned images in editor.
Usability : Add hamburger menu to tool-bar if main menu is hidden.
Metadata  : Add new option to write metadata to files with ExifTool backend.
Metadata  : Optional write metadata operations to DNG and RAW files are now always delegated to ExifTool Backend.

*****************************************************************************************************
BUGFIXES:

001 ==> 416704 - Port digiKam to Qt6 and KF6 API.
002 ==> 450153 - All groups deleted by "Extended clean up and shrink all databases".
003 ==> 450518 - Crash when opening Advanced Search.
004 ==> 450467 - digiKam git source build fails.
005 ==> 450642 - Provide an option to open the light table after click on thumbnail.
006 ==> 450643 - Expose menus optionally in the toolbar : burger main menu feature.
007 ==> 451017 - Search by time of day.
008 ==> 349268 - Be able to copy and paste the uploaded url to ImgUr web service.
009 ==> 451525 - Maintenance tool dialog should not keep potentially destructive operations checked by default.
010 ==> 251424 - Include gpx overlays found in albums.
011 ==> 451802 - GPX files fails to load.
012 ==> 219856 - Exiv2 destroys RAW/DNG files when editing IPTC - Use ExifTool instead.
013 ==> 219856 - Exiv2 destroys RAW/DNG files when editing IPTC - Use ExifTool instead.
014 ==> 366348 - Saving metadata to video files using ExifTool.
015 ==> 377622 - IPTC metadata are not embedded in ORF files with Exiv2 - Use ExifTool instead.
016 ==> 338075 - Tagging RAW images for Canon EOS-1Ds with Exiv2 corrupts them - Use ExifTool instead.
017 ==> 419801 - XMP-digiKam:TagsList not read from video files with ExiV2 - Use ExifTool instead.
018 ==> 134486 - Keywords are not written by Exiv2 to raw files even though they do embed iptc/exif - Use ExifTool instead.
019 ==> 264210 - RAW (NEF) file unreadable after editing IPTC with Exiv2 - Use ExifTool instead.
020 ==> 309341 - Exiv2 doesn't write XMP-tiff Orientation tag for raw files - Use ExifTool instead.
021 ==> 384092 - Writing gps metadata with Exiv2 to sony alpha6300 raw files deletes data (white balance data probably) - Use ExifTool instead.
022 ==> 416516 - Add ExifTool based wrapper to read and write metadata with video files.
023 ==> 326408 - Writing metadata corrupt or is not supported for RAW files using Exiv2 - Use ExifTool instead.
024 ==> 237504 - Impossible change exif, ipct and xmp value in Raw or Orf files from Panasonic or Olympus cameras with Exiv2 (.orf and . raw) - Use ExifTool instead.
025 ==> 406540 - Add Metatags to Canon Raw do not work with Exiv2 - Use ExifTool instead.
026 ==> 426938 - MP4 scans inconsistently chooses wrong "creation date" with ffmpeg - Use ExifTool instead.
027 ==> 436876 - Cannot write exif data to mp4 video files with Exiv2 - Use ExifTool instead.
028 ==> 193228 - Experimental Exiv2 option "write metadata to RAW files" corrupts Nikon NEFs - Use ExifTool instead.
029 ==> 235171 - digiKam crashes when saving ratings to a raw (nef) file - Use ExifTool instead.
030 ==> 236127 - digiKam crashes sometimes, when changing the rating of a rawfile - Use ExifTool instead.
031 ==> 291627 - NEF data loss after geotaging with Exiv2 - Use ExifTool instead.
032 ==> 305823 - digiKam writes to Samsung RAW files with ExifTool.
033 ==> 317745 - Store metadata incl. geolocation data into CR2 files with ExifTool.
034 ==> 234181 - digiKam crashes when editing metadata of CR2 files with Exiv2 - Use ExifTool instead.
035 ==> 366406 - Does not write metadata to ORF with Exiv2 - Use ExifTool instead.
036 ==> 168064 - Save tag on raw files with ExifTool.
037 ==> 134487 - Keywords are not written to raw files with Exiv2 even though they do embed iptc/exif - Use ExifTool instead.
038 ==> 325458 - Editing Exif information corrupts MPO 3D image with Exiv2 - Use ExifTool instead.
039 ==> 448729 - Metadata is not written back to file when size of XMP JPEG segment is larger than 65535 bytes - Use ExifTool instead Exiv2.
040 ==> 451466 - XMP with Google depth-map gets corrupted when metadata is modified in digiKam (likely exiv2 issue) - Use ExifTool instead.
041 ==> 431113 - Apply templates to video files, not only to JPEGs, with ExifTool.
042 ==> 441758 - Editing metadata on HEIC image and HEVC video files doesn't work (greyed out).
043 ==> 440547 - The writing of metadata in HEIC files is not possible - Use ExifTool instead.
044 ==> 434246 - XMP metadata fails to write to file with Exiv2 - Use ExifTool instead.
045 ==> 225272 - digiKam destroy some Exif data with Exiv2 - Use ExifTool instead.
046 ==> 174108 - digiKam and exiv2 crash with AVI file - Use ExifTool instead.
047 ==> 342030 - digiKam crashes when checking an AVI video file using exiv2 - Use ExifTool instead.
048 ==> 171989 - Can't set gps photo location on .nef images with Exiv2 - Use ExifTool instead.
049 ==> 173828 - Geolocation do not except Canon EOS 1D Mark III CR2-files with Exiv2 - Use ExifTool instead.
050 ==> 447371 - Date of videos seems incorrect.
051 ==> 453180 - "not valid" dates for video in "Adjust Time & Date".
052 ==> 453992 - Build error in digiKam Setup.
053 ==> 453630 - Metadata doesn 't always keep it's hierarchy when written with exiftool.
054 ==> 426951 - digiKam audio issue.
055 ==> 
